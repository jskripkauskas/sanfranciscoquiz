package com.sanfranquiz.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;

import com.sanfranquiz.main.UserData;

/**
 * Class for google api client related work.
 */
public class GoogleApiController implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final int RC_SIGN_IN = 9001;
    private GoogleApiClient googleApiClient;
    private GameController gameController;
    private UserData userData;
    private String gameMode;
    private Activity activity;
    private boolean mAutoStartSignInFlow = true;
    private boolean mSignInClicked = false;
    private boolean mResolvingConnectionFailure = false;
    private boolean signedIn = false;
    private String email;

    public GoogleApiController(Activity activity, GameController gameController, String gameMode, UserData userData) {
        init(activity, gameMode);
        this.gameController = gameController;
        this.userData = userData;
    }

    public GoogleApiController(Activity activity, String gameMode) {
        init(activity, gameMode);
    }

    protected void init(Activity activity, String gameMode) {
        googleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
                .addApi(Games.API)
                .addScope(Games.SCOPE_GAMES)
                .build();
        this.activity = activity;
        this.gameMode = gameMode;
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingConnectionFailure) {
            return;
        }

        if (mSignInClicked || mAutoStartSignInFlow) {
            mAutoStartSignInFlow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = resolveConnectionFailure(activity, googleApiClient,
                    connectionResult, RC_SIGN_IN, "There was an issue with sign in. Please try again later.");
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i("googleApi", "connection");
        signedIn = true;
        email = Plus.AccountApi.getAccountName(googleApiClient);
        if (gameController == null) return;

        gameController.requestCurrentHighScore(gameMode);
        //in case we sent score but ended up with connect
        if (userData != null && userData.getScore() > 0) {
            gameController.sendScore(gameMode, signedIn, email);
        }

    }

    public static boolean resolveConnectionFailure(Activity activity,
                                                   GoogleApiClient client, ConnectionResult result, int requestCode,
                                                   String fallbackErrorMessage) {
        Log.i("googleApi", fallbackErrorMessage);

        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(activity, requestCode);
                return true;
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                client.connect();
                return false;
            }
        } else {
            // not resolvable... so show an error message
            int errorCode = result.getErrorCode();
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(errorCode,
                    activity, requestCode);
            if (dialog != null) {
                dialog.show();
            }
            return false;
        }
    }

    public void setmSignInClicked(boolean mSignInClicked) {
        this.mSignInClicked = mSignInClicked;
    }

    public void setmResolvingConnectionFailure(boolean mResolvingConnectionFailure) {
        this.mResolvingConnectionFailure = mResolvingConnectionFailure;
    }

    public boolean isSignedIn() {
        return signedIn;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

    public String getEmail(){
        return email;
    }
}
