package com.sanfranquiz.async;

import android.graphics.drawable.Drawable;

public interface ImageAsyncResponse {
	public void processImageFinish(String id, String name, String address, String latitude, String longitude, Drawable resultImage, int result);
}
