package com.sanfranquiz.app;

import android.app.Application;

import com.flurry.android.FlurryAgent;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class QuizApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FlurryAgent.init(this, "Z4DZ6W25K4W2S57YTZK7");
    }

}