package com.sanfranquiz.async;

import org.json.JSONObject;

public interface DataAsyncResponse {
    void processFinish(JSONObject output);
}
