package com.sanfranquiz.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class QuizProgressBar extends ProgressBar {
    protected int count = 0;
    protected int fileNr = 4;
    protected Bitmap bitmapList[] = new Bitmap[fileNr];
    protected Paint paint;
    protected Handler handler;
    protected Runnable runnable;
    protected int progressWidth;

    public QuizProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public QuizProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public QuizProgressBar(Context context) {
        super(context);
        init(context);
    }

    protected void init(Context context) {
        count = 0;
        paint = new Paint();
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                QuizProgressBar.this.invalidate();
            }
        };

        for (int i = 0; i < fileNr; i++) {
            int resID = getResources().getIdentifier("progress_" + (i + 1), "drawable", context.getPackageName());
            bitmapList[i] = BitmapFactory.decodeResource(getResources(), resID);
        }

        progressWidth = bitmapList[0].getWidth();
    }

    @Override
    public synchronized void onDraw(@NonNull Canvas canvas) {
        canvas.drawBitmap(bitmapList[count], progressWidth / 2, 0.0f, paint);
        count++;
        if (count >= fileNr) {
            count = 0;
        }

        handler.postDelayed(runnable, 600);
    }
}
