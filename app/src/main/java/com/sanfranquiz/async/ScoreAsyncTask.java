package com.sanfranquiz.async;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ScoreAsyncTask extends AsyncTask<String, Void, String> {
	private static String SOURCE_URL = "http://www.insanitydev.com/san_fran/add_score.php";
    private String email = null;
    private Integer score = null;

    public ScoreAsyncTask (String email, Integer score) {
        this.email = email;
        this.score = score;
    }

	@Override
	protected String doInBackground(String... params) {
        Log.d("ScoreAsyncTask", "Sending score");
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(SOURCE_URL);
            List nameValuePairs = new ArrayList();
            nameValuePairs.add(new BasicNameValuePair("email", email));
            nameValuePairs.add(new BasicNameValuePair("score", String.valueOf(score)));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            return new BasicResponseHandler().handleResponse(response);
        } catch (Exception ex) {
            Log.e("SendScore", ex.toString());
        }
        return null;
    }
}
