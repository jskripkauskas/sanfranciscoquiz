package com.sanfranquiz.app;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class QuizDialog extends Dialog {
    private TextView title, description;
    private ImageView thumbnail;
    private ImageButton cancel, accept;
    private String titleText = "", descriptionText = "";
    private Bitmap thumbnailBitmap;
    private int thumbnailId = -1;
    private View.OnClickListener acceptClickListener, cancelClickListener;

    public QuizDialog(Context context) {
        super(context);
    }

    public QuizDialog(Context context, int theme) {
        super(context, theme);
    }

    protected QuizDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.quiz_dialog);
        cancel = (ImageButton) findViewById(R.id.cancel_dialog);
        accept = (ImageButton) findViewById(R.id.accept_dialog);
        title = (TextView) findViewById(R.id.dialog_title);

        description = (TextView) findViewById(R.id.dialog_description);
        thumbnail = (ImageView) findViewById(R.id.dialog_thumbnail);

        setTitle(titleText);
        setDescription(descriptionText);
        setThumbnail(thumbnailBitmap);
        setThumbnail(thumbnailId);
        setOnClickAccept(acceptClickListener);
        setOnClickCancel(cancelClickListener);
    }

    public TextView getTitle() {
        if (title == null) return null;
        
        title.setVisibility(View.VISIBLE);
        return title;
    }

    public void setTitle(String titleText) {
        if (this.title == null){
            this.titleText = titleText;
            return;
        }

        this.title.setVisibility(View.VISIBLE);
        this.title.setText(titleText);
    }

    public TextView getDescription() {
        if (description == null)  return null;
        
        description.setVisibility(View.VISIBLE);
        return description;
    }

    public void setDescription(String descriptionText) {
        if (description == null){
            this.descriptionText = descriptionText;
            return;
        }
        
        description.setVisibility(View.VISIBLE);
        this.description.setText(Html.fromHtml(descriptionText));
    }

    public ImageView getThumbnail() {
        if (thumbnail == null) return null;
        
        thumbnail.setVisibility(View.VISIBLE);
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnailBitmap) {
        if (thumbnail == null || thumbnailBitmap == null){
            this.thumbnailBitmap = thumbnailBitmap;
            return;
        }
        
        thumbnail.setVisibility(View.VISIBLE);
        this.thumbnail.setImageBitmap(thumbnailBitmap);
    }

    public void setThumbnail(int thumbnailId) {
        if (thumbnail == null || thumbnailId < 0){
            this.thumbnailId = thumbnailId;
            return;
        }

        thumbnail.setVisibility(View.VISIBLE);
        this.thumbnail.setImageResource(thumbnailId);
    }

    public ImageButton getCancel() {
        if (cancel == null) return null;

        cancel.setVisibility(View.VISIBLE);
        return cancel;
    }

    public ImageButton getAccept() {
        if (accept == null) return null;

        accept.setVisibility(View.VISIBLE);
        return accept;
    }

    public void setOnClickAccept(View.OnClickListener clickListener){
        if (accept == null || clickListener == null){
            this.acceptClickListener = clickListener;
            return;
        }

        accept.setVisibility(View.VISIBLE);
        accept.setOnClickListener(clickListener);
    }

    public void setOnClickCancel(View.OnClickListener clickListener){
        if (cancel == null || clickListener == null){
            this.cancelClickListener = clickListener;
            return;
        }

        cancel.setVisibility(View.VISIBLE);
        cancel.setOnClickListener(clickListener);
    }

}
