package com.sanfranquiz.main;

/**
 * Created by TAVR on 2015-02-25.
 */
public class Timer {
    private long startTime = 0L;
    private long updatedTime = 0L;
    private long extraTime = 0l;
    private long totalTime = 30*1000;

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public long getExtraTime() {
        return extraTime;
    }
    public void addExtraTime(int time) {
        extraTime = extraTime + time;
    }

    public void setExtraTime(long extraTime) {
        this.extraTime = extraTime;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
