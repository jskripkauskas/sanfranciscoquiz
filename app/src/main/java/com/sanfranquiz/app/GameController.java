package com.sanfranquiz.app;

import android.app.Activity;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sanfranquiz.main.UserData;

/**
 * Class for game elements control, mainly google games service related work.
 */
public class GameController {
    private GoogleApiClient googleApiClient;
    private Activity activity;
    private List<String> achievementArray;
    private UserData userData;
    private long scoreFromServer = -1;

    public GameController(GoogleApiClient googleApiClient, Activity activity, UserData userData) {
        this.googleApiClient = googleApiClient;
        this.activity = activity;
        achievementArray = new ArrayList<>();
        this.userData = userData;
    }

    ResultCallback scoreCallback = new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
        @Override
        public void onResult(Leaderboards.LoadPlayerScoreResult arg0) {
            TextView scoreText = (TextView) activity.findViewById(R.id.score_txt);
            if (arg0 == null) return;
            LeaderboardScore c = arg0.getScore();
            if (c == null) return;
            scoreFromServer = c.getRawScore();
            userData.setHighScore((int) scoreFromServer);
            scoreText.setText("Best Score: " + scoreFromServer);
        }
    };

    /**
     * Displays appropriate leaderboard according to game mode via new activity.
     *
     * @param gameMode String name of current game mode.
     */
    public void displayLeaderboard(String gameMode) {
        if (googleApiClient.isConnected()) {
            if (gameMode.equalsIgnoreCase("CHALLENGE")) {
                activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(googleApiClient, activity.getString(R.string.leaderboard_1)), 1);
            } else if (gameMode.equalsIgnoreCase("HARD")) {
                activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(googleApiClient, activity.getString(R.string.leaderboard_2)), 1);
            }
        } else {
            googleApiClient.connect();
        }
    }

    /**
     * Method unlocks achievements within achievement array and clears an array once achievements are unlocked.
     */
    public void rewardAchievements() {
        if (!googleApiClient.isConnected()) {
            return;
        }

        for (String achievement : achievementArray) {
            Games.Achievements.unlock(googleApiClient, achievement);
        }

        achievementArray.clear();
    }

    /**
     * Adds achievement to unlock later to an achievement list.
     *
     * @param achievement String id of an achievement.
     */
    public void addAchievement(String achievement) {
        achievementArray.add(achievement);
    }

    /**
     * Send score to google and renew current high score.
     * TODO: in case score cannot be sent, save it locally and send it later.
     */
    public void sendScore(String gameMode, Boolean signedIn, String email) {
        if (signedIn && googleApiClient.isConnected()) {

            if (gameMode.equalsIgnoreCase("CHALLENGE")) {
                Games.Leaderboards.submitScore(googleApiClient, activity.getString(R.string.leaderboard_1), userData.getScore());
            } else if (gameMode.equalsIgnoreCase("HARD")) {
                Games.Leaderboards.submitScore(googleApiClient, activity.getString(R.string.leaderboard_2), userData.getScore());
            }

            //send round time to flurry
            Map<String, String> params = new HashMap<>();
            params.put("score",  userData.getScore() + "");
            params.put("mode",  gameMode + "");
            params.put("user",  email + "");
            FlurryAgent.logEvent("Round_score", params);

            if (userData.getScore() > scoreFromServer) {
                requestCurrentHighScore(gameMode);
            }
        } else {
            googleApiClient.connect();
        }
    }

    /**
     * Initiates a request to google games services for users current high score.
     *
     * @param gameMode String name of current game mode.
     */
    public void requestCurrentHighScore(String gameMode) {
        if (googleApiClient.isConnected()) {
            if (gameMode.equalsIgnoreCase("CHALLENGE")) {
                Games.Leaderboards.loadCurrentPlayerLeaderboardScore(googleApiClient, activity.getString(R.string.leaderboard_1),
                        LeaderboardVariant.TIME_SPAN_ALL_TIME, LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(scoreCallback);
            } else if (gameMode.equalsIgnoreCase("HARD")) {
                Games.Leaderboards.loadCurrentPlayerLeaderboardScore(googleApiClient, activity.getString(R.string.leaderboard_2),
                        LeaderboardVariant.TIME_SPAN_ALL_TIME, LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(scoreCallback);
            }
        }
    }

    /**
     * Unlocks specific achievement according to given id.
     *
     * @param id Id of achievement within this app.
     */
    public void unlockAchievement(int id) {
        switch (id) {
            case 5:
                if (googleApiClient.isConnected()) {
                    Games.Achievements.unlock(googleApiClient, activity.getString(R.string.achievement_5));
                }
                break;
        }
    }

    /**
     * Sends specific event according to given id.
     *
     * @param id Id of event within this app.
     */
    public void sendEvent(int id) {
        switch (id) {
            case 1:
                if (googleApiClient.isConnected()) {
                    Games.Events.increment(googleApiClient, activity.getString(R.string.event_1), 1);
                }
                break;
            case 2:
                if (googleApiClient.isConnected()) {
                    Games.Events.increment(googleApiClient, activity.getString(R.string.event_2), 1);
                }
                break;
        }
    }

}
