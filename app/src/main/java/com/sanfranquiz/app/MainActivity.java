package com.sanfranquiz.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.api.GoogleApiClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.sanfranquiz.async.DataAsyncResponse;
import com.sanfranquiz.async.DataAsyncTask;
import com.sanfranquiz.async.ImageAsyncResponse;
import com.sanfranquiz.async.ImageGetterAsyncTask;
import com.sanfranquiz.main.NavDrawerItem;
import com.sanfranquiz.main.NavDrawerListAdapter;
import com.sanfranquiz.main.Timer;
import com.sanfranquiz.main.UserData;
import com.sanfranquiz.tinderLike.model.CardModel;
import com.sanfranquiz.tinderLike.utils.OnCardDimissedListener;
import com.sanfranquiz.tinderLike.utils.OnClickListener;
import com.sanfranquiz.tinderLike.view.CardContainer;
import com.sanfranquiz.tinderLike.view.QuizTextView;
import com.sanfranquiz.tinderLike.view.SimpleCardStackAdapter;

public class MainActivity extends Activity implements DataAsyncResponse, ImageAsyncResponse {
    //---------------VARIABLES----------------//
    private CardContainer mCardContainer;
    private SimpleCardStackAdapter adapter;
    //for online
    public DataAsyncTask dataGetter = null;
    private static UserData userData = null;
    private Timer timer = null;
    private QuizTextView timerValue, scoreValue;
    //protected int current = 0;
    protected boolean isFirst = true;
    private String gameMode = "CHALLENGE";
    private QuizTextView answerValue;
    private Handler customHandler = new Handler();
    //Drawer
    private String[] navMenuTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    //MainCardsData
    //private int highScore = 1;
    //offline cards data
    private JSONArray offlineCardsData = null;
    //online cards data
    private JSONArray onlineCardsData = null;
    //used to synchronize online with offline content
    private List<CardModel> onlineCardsToAdd = new LinkedList<>();
    private int cardsCorrect = 0, cardsWrong = 0, loseStreak = 0;
    private final static int MAX_TIME = 59 * 1000;
    private RelativeLayout beginMenu;
    private GameController gameController;
    private GoogleApiController googleApiController;
    private PushNotificationController pushNotificationController;
    /*private ViewGroup progressContainer;
    private View progressBar;*/

    //--------------LIFECYCLE----------------//

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //overridePendingTransition(R.anim.slidein_left_view, R.anim.slideout_left_view);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainlayout);
        // init push notification controller
        pushNotificationController = new PushNotificationController(this);
       /* progressContainer = (ViewGroup) findViewById(R.id.drawer_layout);
        progressBar = getLayoutInflater().inflate(R.layout.progress_overlay, progressContainer, false);*/

        adapter = new SimpleCardStackAdapter(this);
        getIntentData();
        mCardContainer = (CardContainer) findViewById(R.id.layoutview);
        mCardContainer.setVisibility(View.GONE);

        if (gameMode.equalsIgnoreCase("CHALLENGE") || gameMode.equalsIgnoreCase("HARD")) {
            timer = new Timer();
            scoreValue = (QuizTextView) findViewById(R.id.score);
            timerValue = (QuizTextView) findViewById(R.id.timerValue);
        }

        // create google api controller (we will set game controller and user later)
        googleApiController = new GoogleApiController(this, gameMode);
        QuizTextView modeTxt = (QuizTextView) findViewById(R.id.mode_txt);
        modeTxt.setText(gameMode + " MODE");
        beginMenu = (RelativeLayout) findViewById(R.id.begin_menu_layout);
        answerValue = (QuizTextView) findViewById(R.id.answerValue);
        getData();
        createDrawerMenu();
        gameController = new GameController(googleApiController.getGoogleApiClient(), this, userData);
        // set google api controller user and game controller
        googleApiController.setUserData(userData);
        googleApiController.setGameController(gameController);
    }

    @Override
    public void onResume() {
        super.onResume();
        pushNotificationController.checkPlayServices();
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiController.getGoogleApiClient().connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        GoogleApiClient googleApiClient = googleApiController.getGoogleApiClient();

        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == GoogleApiController.RC_SIGN_IN) {
            googleApiController.setmSignInClicked(false);
            googleApiController.setmResolvingConnectionFailure(false);

            if (responseCode == RESULT_OK) {
                googleApiController.getGoogleApiClient().connect();
            }
        }
    }

    //---------------OBJECTS--------------//

    Runnable hideAnswer = new Runnable() {
        @Override
        public void run() {
            QuizAnimator.collapse(answerValue);
        }
    };

    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            long updatedTime = timer.getTotalTime() + timer.getExtraTime() - (SystemClock.uptimeMillis() - timer.getStartTime());
            if (updatedTime > MAX_TIME) {
                timer.setExtraTime(MAX_TIME - timer.getTotalTime() + (SystemClock.uptimeMillis() - timer.getStartTime()));
                updatedTime = MAX_TIME;
            }

            timer.setUpdatedTime(updatedTime);
            if (updatedTime < 0) {
                timer.setExtraTime(0l);
                customHandler.removeCallbacks(updateTimerThread);
                timerValue.setText(""
                        + String.format("%02d", 0) + ":"
                        + String.format("%02d", 0));
                CharSequence text = "You collected <b>" + userData.getScore() + "</b>!!";
                addDialog(text.toString(), "Congratulations!");
                gameController.sendScore(gameMode, googleApiController.isSignedIn(), "");
                userData.setScore(1);
                scoreValue.setText("Your score is: " + userData.getScore());
                timerValue.setText(""
                        + String.format("%02d", 30));
                userData.setStreak(0);
            } else {
                int secs = (int) (updatedTime / 1000);
                secs = secs % 60;
                timerValue.setText(""
                        + String.format("%02d", secs));
                customHandler.postDelayed(this, 0);
            }
        }
    };

    //----------------MAIN----------------//
/*
    private void addProgress(final View progressBar){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressBar.getParent() != null) {
                    progressContainer.removeView(progressBar);
                }
                progressContainer.addView(progressBar, progressContainer.getChildCount(), new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
            }
        });
    }*/

    private void getIntentData() {
        Intent intent = getIntent();
        userData = new UserData("");

        if (intent.getStringExtra("gameMode") != null) {
            gameMode = intent.getStringExtra("gameMode");
        }

        pushNotificationController.showPushNotificationData(intent);
    }

    private void createDrawerMenu() {
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        TypedArray navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer);
        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();

        // adding nav drawer items to array
        // Home
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        if (Utils.isConnected(this)) {
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        }
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
        // Recycle the typed array
        navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        NavDrawerListAdapter adapter2 = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter2);
        mDrawerList.bringToFront();
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    private void displayView(final int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                startNextModeDelayed("DISCOVERY");
                break;
            case 1:
                startNextModeDelayed("CHALLENGE");
                break;
            case 2:
                startNextModeDelayed("HARD");
                break;
            case 3:
                gameController.displayLeaderboard(gameMode);
                break;
            case 4:
                FacebookShareFragment fbFragment = new FacebookShareFragment();
                fbFragment.setName("Vilnius Quiz");
                fbFragment.setCaption("I know Vilnius");
                fbFragment.setDescription("Achieved high score of " + userData.getHighScore() + " in Vilnius Quiz game!");
                fbFragment.setLink("https://play.google.com/store/apps/details?id=com.sanfranquiz.app&hl=en");
                fragment = fbFragment;
                gameController.unlockAchievement(5);
                break;
            case 5:
                GoogleShareFragment googleShareFragment = new GoogleShareFragment();
                googleShareFragment.setShareText("I have achieved high score of " + userData.getHighScore() + " in Vilnius Quiz game!");
                googleShareFragment.setContentUrl("https://play.google.com/store/apps/details?id=com.sanfranquiz.app&hl=en");
                fragment = googleShareFragment;
                gameController.unlockAchievement(5);
                break;
            default:
                break;
        }


        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
        }

        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
        setTitle(navMenuTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);

        customHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDrawerList.setItemChecked(position, false);
                mDrawerList.setSelection(0);
            }
        }, 500l);
    }

    /**
     * Send intent to start new game mode after a short delay.
     * @param gameMode Game mode to start.
     */
    protected void startNextModeDelayed(final String gameMode){
        /*addProgress(progressBar);
        customHandler.postDelayed(new Runnable() {
            @Override
            public void run() {*/
                Intent nextIntent = getIntent();
                nextIntent.putExtra("gameMode", gameMode);
                startActivity(nextIntent);
                finish();
/*            }
        }, 1000l);*/
    }

    //Commented for Online mode
    public void getOnlineData(){
        Random rand = new Random();
        int maxRandomValue = 1;
        int minRandomValue = 1;
        switch (gameMode) {
            case "CHALLENGE":
                maxRandomValue = 3;
                minRandomValue = 1;
                break;
            case "HARD":
                maxRandomValue = 5;
                minRandomValue = 1;
                break;
            default:
                maxRandomValue = 2;
                minRandomValue = 1;
                break;
        }

        int randomNum = rand.nextInt(maxRandomValue) + minRandomValue;
        Log.d("getOnlineData:email:val", "email: "+googleApiController.getEmail()+" difficulty: "+ randomNum);
        DataAsyncTask dataGetter2 = new DataAsyncTask(googleApiController.getEmail(), randomNum);
        dataGetter2.delegate = this;
        dataGetter2.execute();
        dataGetter = dataGetter2;
    }

    //Offline mode
    public void getData() {
        if (offlineCardsData == null) {
            try {
                JSONObject cardsDataObject = new JSONObject(loadJSONFromAsset());
                offlineCardsData = (JSONArray) cardsDataObject.get("data");
                shuffleOfflineJsonArray();
            } catch (Exception ex) {
                Log.d("getData", "Bad data2" + ex.toString());
            }
        }
//        if (googleApiController.isSignedIn()) {
            if (onlineCardsData == null || onlineCardsData.length() == 0) {
                getOnlineData();
            } else if (onlineCardsData != null && onlineCardsData.length() != 0 && onlineCardsToAdd.size() < 3) {
                try {
                    JSONObject obj = onlineCardsData.getJSONObject(onlineCardsData.length() - 1);
                    String address = obj.get("address") != null? obj.get("address").toString(): null;
                    ImageGetterAsyncTask temp = new ImageGetterAsyncTask(obj.getString("id"), (String) obj.get("name"),
                            address, (String) obj.get("lng"), (String) obj.get("lat"), (String) obj.get("picture"), obj.getInt("result"));
                    removeOnlineJsonObjectFromArray(onlineCardsData.length() - 1);
                    temp.delegate = this;
                    temp.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//        }
        if (onlineCardsToAdd.isEmpty()) {
            try {
                JSONObject obj = offlineCardsData.getJSONObject(offlineCardsData.length() - 1);
                String address = obj.get("address") != null? obj.get("address").toString(): null;
                processDataCard(obj.getString("id"), (String) obj.get("name"),
                        address, (String) obj.get("lat"), (String) obj.get("lng"), loadImage((String) obj.get("picture")), obj.getInt("result"));
                removeOfflineJsonObjectFromArray(offlineCardsData.length() - 1);
            } catch (Exception ex) {
                offlineCardsData = null;
                getData();
                Log.d("getData", "Bad data3" + ex.toString());
            }
        } else {
            Log.d("OnlineCardGetter", "onlineCard");
            adapter.addLast(onlineCardsToAdd.get(onlineCardsToAdd.size()-1));
            onlineCardsToAdd.remove(onlineCardsToAdd.size()-1);
            if (isFirst) {
                isFirst = false;
                mCardContainer.setAdapter(adapter);
            }
        }
    }

    //Online mode getter [DataAsyncTask result]
    @Override
    public void processFinish(JSONObject output) {
       if (output != null) {
           try {
               onlineCardsData = (JSONArray) output.get("data");
               shuffleOnlineJsonArray();
               JSONObject obj = onlineCardsData.getJSONObject(onlineCardsData.length() - 1);
               Log.d("Online::processFinish", obj.toString());
               ImageGetterAsyncTask temp = new ImageGetterAsyncTask(obj.getString("id"), (String) obj.get("name"),
                       (String) obj.get("address"), (String) obj.get("lng"), (String) obj.get("lat"), (String) obj.get("picture"), obj.getInt("result"));
               removeOnlineJsonObjectFromArray(onlineCardsData.length() - 1);
               temp.delegate = this;
               temp.execute();
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
    }

    //Online mode final (2) getter
    @Override
    public void processImageFinish(String id, String name, String address, String latitude, String longitude, Drawable resultImage, int result) {
        CardModel cardModel = new CardModel(id, "Is it " + name + "?", address, latitude, longitude, resultImage, result);
        cardModel.setOnClickListener(new OnClickListener() {
            @Override
            public void OnClickListener() {
                timerStarter(null);
                Log.i("Swipeable Cards", "I am pressing the card");
            }
        });

        cardModel.setOnCardDimissedListener(new OnCardDimissedListener() {
            @Override
            public void onYes() {
                getData();
                if (adapter.getCount() != 0) {
                    CardModel cardModel = adapter.pop();
                    mCardContainer.updateAdapter(adapter);
                    countScore(cardModel.getResult(), 1);
                    Log.i("Swipeable Cards", "Yes to the card");
                }
            }

            @Override
            public void onNo() {
                getData();
                if (adapter.getCount() != 0) {
                    CardModel cardModel = adapter.pop();
                    mCardContainer.updateAdapter(adapter);
                    countScore(cardModel.getResult(), 0);
                    Log.i("Swipeable Cards", "No to the card");
                }
            }
        });
        onlineCardsToAdd.add(cardModel);
    }

    public void processDataCard(String id, String name, String address, String latitude, String longitude, Drawable image, int result) {
        CardModel cardModel = new CardModel(id, "Is it " + name + "?", address, latitude, longitude, image, result);
        cardModel.setOnClickListener(new OnClickListener() {
            @Override
            public void OnClickListener() {
                timerStarter(null);
                Log.i("Swipeable Cards", "I am pressing the card");
            }
        });

        cardModel.setOnCardDimissedListener(new OnCardDimissedListener() {
            @Override
            public void onYes() {
                getData();
                if (adapter.getCount() != 0) {
                    CardModel cardModel = adapter.pop();
                    mCardContainer.updateAdapter(adapter);
                    countScore(cardModel.getResult(), 1);
                    Log.i("Swipeable Cards", "Yes to the card");
                }
            }

            @Override
            public void onNo() {
                getData();
                if (adapter.getCount() != 0) {
                    CardModel cardModel = adapter.pop();
                    mCardContainer.updateAdapter(adapter);
                    countScore(cardModel.getResult(), 0);
                    Log.i("Swipeable Cards", "No to the card");
                }
            }
        });
        adapter.addLast(cardModel);
        if (isFirst) {
            isFirst = false;
            mCardContainer.setAdapter(adapter);
        }
    }

    public void fourSquare(View view) {
        CardModel currentModel = adapter.getCardModel(adapter.getCount() - 1);
        Uri uriUrl = Uri.parse("https://foursquare.com/venue/" + currentModel.getId());
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void googleMaps(View view) {
        CardModel currentModel = adapter.getCardModel(adapter.getCount() - 1);
        String description = currentModel.getDescription();
        if (description != null) {
            description = "?q=101+" + description.replaceAll(" ", "+");
        }
        Uri gmmIntentUri = Uri.parse("geo:" + currentModel.getLatitude() + "," + currentModel.getLongitude() + description);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    public void sideMenu(View view) {
        Log.i("clickListen", "clicked");
        mDrawerLayout.openDrawer(mDrawerList);
    }

    public void answerYes(View view) {
        getData();//TODO cardModel.performSwipe
        if (adapter.getCount() != 0) {
            CardModel cardModel = adapter.popSwipe();
            mCardContainer.updateAdapter(adapter);
            countScore(cardModel.getResult(), 1);
        }
        timerStarter(view);
    }

    public void answerNo(View view) {
        getData();//TODO cardModel.performSwipe
        if (adapter.getCount() != 0) {
            CardModel cardModel = adapter.popSwipe();
            mCardContainer.updateAdapter(adapter);
            countScore(cardModel.getResult(), 0);
        }
        timerStarter(view);
    }

    public void answerInfo(View view) {
        timerStarter(view);
    }

    public void begin(View view) {
        beginMenu.setVisibility(View.GONE);
        mCardContainer.setVisibility(View.VISIBLE);
        findViewById(R.id.yes).setVisibility(View.VISIBLE);
        findViewById(R.id.no).setVisibility(View.VISIBLE);

        if (gameMode.equalsIgnoreCase("CHALLENGE") || gameMode.equalsIgnoreCase("HARD")) {
            findViewById(R.id.score).setVisibility(View.VISIBLE);
            findViewById(R.id.timerValue).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.score).setVisibility(View.GONE);
            findViewById(R.id.timerValue).setVisibility(View.GONE);
        }
        findViewById(R.id.answerValue).setVisibility(View.VISIBLE);
        timerStarter(null);
    }

    public void gameOver() {
        beginMenu.setVisibility(View.VISIBLE);
        mCardContainer.setVisibility(View.GONE);
        findViewById(R.id.yes).setVisibility(View.GONE);
        findViewById(R.id.no).setVisibility(View.GONE);
        findViewById(R.id.score).setVisibility(View.GONE);
        findViewById(R.id.timerValue).setVisibility(View.GONE);
        findViewById(R.id.answerValue).setVisibility(View.GONE);
        //send round time to flurry
        Map<String, String> params = new HashMap<>();
        params.put("time", timer.getTotalTime() + timer.getExtraTime() + "");
        FlurryAgent.logEvent("Round_time", params);
        gameController.rewardAchievements();
    }

    protected void countScore(int expected, int result) {
        CharSequence text;
        int textColor;
        int difficulty = 2;
        if (gameMode.equalsIgnoreCase("HARD")) {
            difficulty = 4;
        }

        if (gameMode.equalsIgnoreCase("CHALLENGE") || gameMode.equalsIgnoreCase("HARD")) {
            if (expected == result) {
                FlurryAgent.logEvent("Correct_answer", null);
                userData.addScore(2);
                userData.addStreak();
                loseStreak = 0;
                cardsCorrect++;
                timer.addExtraTime(1000);
                textColor = Color.GREEN;
                text = "Correct! (+1s)(+2score)";
                gameController.sendEvent(1);
            } else {
                FlurryAgent.logEvent("Incorrect_answer", null);
                userData.setStreak(0);
                userData.addScore(-2 * difficulty);
                textColor = Color.RED;
                loseStreak++;
                cardsWrong++;
                timer.addExtraTime(-1000 * difficulty);
                text = "Incorrect! (-" + (2 * difficulty) + "score)(-" + difficulty + "s)";
                gameController.sendEvent(2);
            }
            int streak = userData.getStreak();
            if (streak % 3 == 0 && streak != 0) {
                timer.addExtraTime(1000);
                userData.addScore(3);
                textColor = Color.GREEN;
                text = streak + " in a row! (+1s)(+3 score)";
            }
            if (loseStreak % 3 == 0 && loseStreak != 0) {
                timer.addExtraTime(-1000 * difficulty);
                userData.addScore(-3 * difficulty);
                textColor = Color.RED;
                text = streak + " in a row! (-" + difficulty + "s)(-" + (3 * difficulty) + " score)";
            }
            if (streak % 5 == 0 && streak != 0) {
                timer.addExtraTime(5 * 1000);
                textColor = Color.GREEN;
                text = streak + " in a row! (+5s)";
            }
            if (streak % 15 == 0 && streak != 0) {
                timer.addExtraTime(10 * 1000);
                userData.addScore(10);
                textColor = Color.GREEN;
                text = streak + " in a row! (+10s)(+10 score)";
            }
            if (loseStreak % 5 == 0 && loseStreak != 0) {
                timer.addExtraTime(-5 * 1000 * difficulty);
                textColor = Color.RED;
                text = streak + " in a row! (-" + (5 * difficulty) + "s)";
            }
            if (loseStreak % 15 == 0 && loseStreak != 0) {
                timer.addExtraTime(-10 * 1000 * difficulty);
                userData.addScore(-10 * difficulty);
                textColor = Color.RED;
                text = streak + " in a row! (-" + (10 * difficulty) + "s)(-" + (10 * difficulty) + " score)";
            }
            int score = userData.getScore();
            if (score > userData.getHighScore()) {
                userData.setHighScore(score);
            }

            if (score < 0) {
                userData.setScore(1);
                score = userData.getScore();
            }
            scoreValue.setText("Your score is: " + score);
        } else {
            if (expected == result) {
                text = "Correct!";
                textColor = Color.GREEN;
            } else {
                text = "Incorrect!";
                textColor = Color.RED;
            }
        }

        Random random = new Random();
        int angle = random.nextInt(50) - 25;
        answerValue.setText(text);
        answerValue.setTextColor(textColor);
        answerValue.setRotation(angle);
        QuizAnimator.expand(answerValue);

        customHandler.removeCallbacks(hideAnswer);
        customHandler.postDelayed(hideAnswer, 1000l);
        calculateAchievements();
    }

    public void addDialog(String message, String title) {
        if (!this.isFinishing()) {
            final QuizDialog dialog = new QuizDialog(this);
            dialog.setTitle(title);
            dialog.setCancelable(false);
            dialog.setDescription(message);
            dialog.setOnClickAccept(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    gameOver();
                }
            });
            dialog.show();
        }
    }

    public void timerStarter(View v) {
        if (timer != null) {
            if (timer.getUpdatedTime() <= 0) {
                timer.setStartTime(SystemClock.uptimeMillis());
                customHandler.postDelayed(updateTimerThread, 0);
            }
        }
    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("data_for_cards.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception ex) {
            Log.e("loadJSONFromAsset", ex.getLocalizedMessage());
            return null;
        }
        return json;
    }

    public JSONArray shuffleOfflineJsonArray() throws JSONException {
        // Implementing FishersYates shuffle
        Random rnd = new Random();
        for (int i = offlineCardsData.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            // Simple swap
            Object object = offlineCardsData.get(j);
            offlineCardsData.put(j, offlineCardsData.get(i));
            offlineCardsData.put(i, object);
        }
        return offlineCardsData;
    }

    public JSONArray shuffleOnlineJsonArray() throws JSONException {
        // Implementing FishersYates shuffle
        Random rnd = new Random();
        for (int i = onlineCardsData.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            // Simple swap
            Object object = onlineCardsData.get(j);
            onlineCardsData.put(j, onlineCardsData.get(i));
            onlineCardsData.put(i, object);
        }
        return onlineCardsData;
    }

    public Drawable loadImage(String url) {
        // load image
        try {
            // get input stream
            InputStream ims = getAssets().open("pictures/" + url);
            // load image as Drawable
            return Drawable.createFromStream(ims, null);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
            //return null;
        }
    }

    public void removeOfflineJsonObjectFromArray(int pos) {
        JSONArray njArray = new JSONArray();
        try {
            for (int i = 0; i < offlineCardsData.length(); i++) {
                if (i != pos)
                    njArray.put(offlineCardsData.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        offlineCardsData = njArray;
    }

    public void removeOnlineJsonObjectFromArray(int pos) {
        JSONArray njArray = new JSONArray();
        try {
            for (int i = 0; i < onlineCardsData.length(); i++) {
                if (i != pos)
                    njArray.put(onlineCardsData.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        onlineCardsData = njArray;
    }

    /**
     * Method calculates whether player deserves an achievement and adds achievements to the
     * list for unlocking them later.
     */
    private void calculateAchievements() {
        if (cardsCorrect + cardsWrong >= 500) {
            gameController.addAchievement(getString(R.string.achievement_1));
        }

        if (cardsCorrect >= 50 && cardsWrong >= 50 && cardsCorrect == cardsWrong) {
            gameController.addAchievement(getString(R.string.achievement_2));
        }

//        if (cardsCorrect + cardsWrong >= 75) {
//            gameController.addAchievement(getString(R.string.achievement_3));
//        }

        if (loseStreak > 10) {
            gameController.addAchievement(getString(R.string.achievement_4));
        }
    }

    //--------------INNER-CLASS----------//

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            displayView(position);
        }
    }
}