package com.sanfranquiz.app;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;

public class FacebookShareFragment extends Fragment {
    protected View view;
    private SessionState state = SessionState.CLOSED;
    public Session session;
    public UiLifecycleHelper uiHelper;
    protected Activity activity;
    protected String name = "", caption = "", description = "", link = "", picture = "";

    Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state);
        }
    };

    private Session.StatusCallback callbackPost = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {

            if (state.isOpened()) {
                publishFeedDialog();
            }
        }
    };

    public FacebookShareFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_facebook_share, container, false);
        view.setVisibility(View.INVISIBLE);
        activity = this.getActivity();
        uiHelper = new UiLifecycleHelper(activity, callback);
        uiHelper.onCreate(savedInstanceState);

        ImageButton shareButton = (ImageButton) view.findViewById(R.id.facebook_share_button);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (FacebookDialog.canPresentShareDialog(activity.getApplicationContext(),
                        FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
                    FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(activity)
                            .setName(name)
                            .setCaption(caption)
                            .setDescription(description)
                            .setLink(link)
                            .build();
                    uiHelper.trackPendingDialogCall(shareDialog.present());
                } else {
                    publishFeedDialog();
                }
            }
        });

        Session session = Session.getActiveSession();
        if (session != null && (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState());
        }
        view.findViewById(R.id.facebook_share_button).performClick();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Session session = Session.getActiveSession();
        if (session != null && (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState());
        }
        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        uiHelper.onActivityResult(requestCode, resultCode, intent, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("Activity", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("Activity", "Success!");
            }
        });
    }

    private void publishFeedDialog() {
        Bundle params = new Bundle();
        params.putString("name", name);
        params.putString("caption", caption);
        params.putString("description", description);
        params.putString("link", link);

        if (Session.getActiveSession() == null || !Session.getActiveSession().isOpened()) {
            Session.openActiveSession(activity, true, callbackPost);
            return;
        }
        session = Session.getActiveSession();

        if (session.isClosed()){
            return;
        }

        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(getActivity(),
                        session, params))
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values, FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
                            final String postId = values.getString("post_id");
                            if (postId != null) {
                                Log.i("facebook share", "shared");
                            } else {
                                // User clicked the Cancel button
                                Log.i("facebook share", "cancel share");
                            }
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Log.i("facebook share", "closed share");
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(getActivity().getApplicationContext(),
                                    "Error posting story",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                })
                .build();
        feedDialog.show();
    }

    /**
     * Method called when session state changes.
     */
    private void onSessionStateChange(Session session, SessionState state) {
        this.state = state;
        this.session = session;

        if (state.isOpened()) {
            Session.setActiveSession(session);
            onSessionOpen();
        }
    }

    /**
     * Method called when session state is "Open".
     */
    private void onSessionOpen() {
        if (state.isOpened()) {
            Session.setActiveSession(session);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
