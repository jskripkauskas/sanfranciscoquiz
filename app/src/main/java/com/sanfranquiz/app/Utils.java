package com.sanfranquiz.app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Various utility methods accessed statically.
 */
public class Utils {

    /**
     * Returns true if has connection to network and false otherwise.
     *
     * @param context A context to search for network within (an activity).
     * @return True if there is network, false otherwise.
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
