package com.sanfranquiz.main;

/**
 * Created by TAVR on 2015-02-25.
 */
public class UserData {

    private String email;
    private int score = 1;
    private int highScore = 1;
    private int streak = 0;

    public UserData (String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void addScore(int scorePoints) {
        score = score + scorePoints;

        if (score < 0){
            score = 0;
        }
    }

    public void reduceScore() {
        score--;
    }

    public void addStreak() {
        streak++;
    }

    public int getStreak() {
        return streak;
    }

    public void setStreak(int streak) {
        this.streak = streak;
    }

    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

}
