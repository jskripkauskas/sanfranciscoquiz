package com.sanfranquiz.tinderLike.utils;

public interface OnCardDimissedListener {
	void onYes();
    void onNo();
}
